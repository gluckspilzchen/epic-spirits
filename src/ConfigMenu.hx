import dn.Process; 
import hxd.Key;

import ui.Components;


@:uiComp("config-item")
class ConfigItemComp extends h2d.Flow implements h2d.domkit.Object {

	static var SRC =
	<config-item class={id} >
		<text text = {label} />	
	</config-item>;


	public function new(id:String, label:String, ?parent) {
		super(parent);
		initComponent();
	}

}

@:uiComp("slider-item")
class SliderItemComp extends h2d.Flow implements h2d.domkit.Object {

	static var SRC =
	<slider-item class={id} >
		<text text = {label} />	
	</slider-item>;

	public function new(id:String, label:String, ?parent) {
		super(parent);

		var slider = new h2d.Slider(this);
		slider.value = Assets.getVolume();
		slider.onChange = function () {
			Assets.setVolume(slider.value);
		}
		initComponent();
	}

}

@:uiComp("config-menu")
class ConfigMenuContainer extends h2d.Flow implements h2d.domkit.Object {

	static var SRC = <config-menu>
		<slider-item("volume-item", "Volume") />
	</config-menu>;

	public function new(align:h2d.Flow.FlowAlign, ?parent) {
		super(parent);
		initComponent();
	}

}


class ConfigMenu extends dn.Process {
	var ca : dn.heaps.Controller.ControllerAccess;
	var mask : h2d.Bitmap;

	var back : dn.Process;

	var center : h2d.Flow;
	var style = null;

	public function new(back: dn.Process) {
		super(Main.ME);

		this.back = back;

		createRootInLayers(Main.ME.root, Const.DP_UI);
		root.filter = new h2d.filter.ColorMatrix(); // force pixel perfect rendering
		var tf = new h2d.Text(Assets.fontLarge, root);
		tf.text = "Config";

		ca = Main.ME.controller.createAccess("config", true);
		back.pause();

		mask = new h2d.Bitmap(hxd.Res.title.toTile(), root);
		root.under(mask);

		createChildProcess(
				function(c) {
					// Resize dynamically
					tf.setScale( M.imax(1, Math.floor( w()*0.05 / tf.textWidth )) );
					tf.x = Std.int( w()*0.5 - tf.textWidth*tf.scaleX*0.5 );
					tf.y = Std.int( h()*0.1 - tf.textHeight*tf.scaleY*0.5 );

				}, true
				);

		center = new h2d.Flow(root);
		center.horizontalAlign = center.verticalAlign = Middle;
		onResize();

		var root = new ConfigMenuContainer(Right, center);

		style = new h2d.domkit.Style();
		style.load(hxd.Res.style);
		style.addObject(root);

		dn.Process.resizeAll();
	}


	override function onResize() {
		super.onResize();

		root.scale(Const.UI_SCALE);

		mask.scaleX = M.ceil( w()/Const.UI_SCALE );
		mask.scaleY = M.ceil( h()/Const.UI_SCALE );

		center.minWidth = center.maxWidth = w();
		center.minHeight = center.maxHeight = h();
	}

	override function onDispose() {
		super.onDispose();
		ca.dispose();
		back.resume();
	}

	public function close() {
		if( !destroyed ) {
			destroy();
		}
	}

	override function update() {
		super.update();

		style.sync();

		if( ca.startPressed() || ca.isKeyboardPressed(hxd.Key.ESCAPE) )
			close();
	}
}
