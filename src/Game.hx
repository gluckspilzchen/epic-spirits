import dn.Process;
import hxd.Key;

class Game extends Process {
	public static var ME : Game;

	public var ca : dn.heaps.Controller.ControllerAccess;
	public var fx : Fx;
	public var camera : Camera;
	public var scroller : h2d.Layers;
	public var level : Level;
	public var hud : ui.Hud;

	public var score : Int;

	var slowMoCd : Float;

	public function new() {
		super(Main.ME);
		ME = this;
		ca = Main.ME.controller.createAccess("game");
		ca.setLeftDeadZone(0.2);
		ca.setRightDeadZone(0.2);
		createRootInLayers(Main.ME.root, Const.DP_BG);

		scroller = new h2d.Layers();
		root.add(scroller, Const.DP_BG);
		scroller.filter = new h2d.filter.ColorMatrix(); // force rendering for pixel perfect

		camera = new Camera();
		level = new Level();
		fx = new Fx();
		hud = new ui.Hud();

		var gdata = Data.entities.get(game);
		slowMoCd = gdata.slowMoCd;

		new ent.Hero(5, 5);

		new ent.Mob(10, 10);

		score = 0;

		Process.resizeAll();
	}

	public function onCdbReload() {
		restartLevel();
	}

	override function onResize() {
		super.onResize();
		scroller.setScale(Const.SCALE);
	}

	public function restartLevel(){
		this.destroy();
		Main.ME.delayer.addF(function() {
			new Game();
		}, 1);
	}

	public function gameOver(){
		this.destroy();
		Main.ME.restart();
	}

	function spawnDZSpawner() {
		var h = level.hei - 2;
		var w = level.wid - 2;

		var x = Math.floor( Math.random() * w);
		var y = 0;

		new ent.DZSpawner(x, y);
	}

	public function bumpScore() {
		score++;

		hud.invalidate();
	}

	public function slowMo() {
		timeMultiplier = 0.5;
		cd.setS("slowMoCd", slowMoCd);
		for (e in Entity.ALL) {
			e.spr.anim.setGlobalSpeed(timeMultiplier);
		}
	}

	public function stopSlowMo() {
		timeMultiplier = 1;
		for (e in Entity.ALL) {
			e.spr.anim.setGlobalSpeed(timeMultiplier);
		}
	}

	public function isSlowMo() {
		return timeMultiplier == 0.5;
	}

	function gc() {
		if( Entity.GC==null || Entity.GC.length==0 )
			return;

		for(e in Entity.GC)
			e.dispose();
		Entity.GC = [];
	}

	override function onDispose() {
		super.onDispose();

		fx.destroy();
		for(e in Entity.ALL)
			e.destroy();
		gc();
	}

	override function preUpdate() {
		super.preUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.preUpdate();
	}

	override function postUpdate() {
		super.postUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.postUpdate();
		gc();
	}

	override function fixedUpdate() {
		super.fixedUpdate();

		for(e in Entity.ALL) if( !e.destroyed ) e.fixedUpdate();
	}

	override function update() {
		super.update();

		if(!cd.has("slowMoCd"))
			stopSlowMo();

		for(e in Entity.ALL) if( !e.destroyed ) e.update();

		if( !ui.Console.ME.isActive() && !ui.Modal.hasAny() ) {
			if( ca.isKeyboardPressed(Key.ESCAPE) )
				new PauseMenu();
		}
	}
}

