package ent;

import haxe.ds.Option;

enum State {
	Stand;
	Run;
	Dash(x:Float, y:Float);
	Block;
	Pary;
}

class Hero extends Entity {
	public static var ME : Null<Hero>;
	var ca : dn.heaps.Controller.ControllerAccess;

	var speed : Float;
	var dashSpeed : Float;
	var dashTime : Float;

	var state: State;
	var oldState: State;
	var oldBlocked: Array<Projectile>;

	var hasUnblock = false;

	public function new (x, y) {
		super(x, y);

		ME = this;

		var hdata = Data.entities.get(hero);
		speed = hdata.speed;
		dashSpeed = hdata.dashSpeed;
		dashTime = hdata.dashTime;

		var g = new h2d.Graphics(spr);
		g.beginFill(0xff00ff);
		g.drawCircle(0,0,radius);

		frict = 0.50;

		state = Stand;
		oldState = Stand;

		oldBlocked = [];

		ca = Main.ME.controller.createAccess("hero");
	}

	public function hit() {
		switch (state) {
			case Dash (_):
			default: Game.ME.gameOver();
		}
	}

	override function dispose() {
		super.dispose();
		ca.dispose();
	}

	function startDash(dirx, diry) {
		cd.setS("dashTime", dashTime);

		return if (M.dist(0, 0, dirx, diry) > 0.05) {
			Dash(dirx, diry);
		} else
			Dash(0, 1);
	}

	override function update() {
		super.update();

		debug(Std.int(hxd.Timer.fps())+" tmod="+pretty(tmod,2));

		var dirx = ca.lxValue();
		var diry = ca.lyValue();
		dirx /= Math.sqrt(dirx * dirx + diry * diry);
		diry /= Math.sqrt(dirx * dirx + diry * diry);

		if (ca.xDown() && hasUnblock)
			state =  Block;
		else if(!ca.xDown())
			hasUnblock = true;


		var blocked = [];
		for (e in Projectile.ALL)
			if (checkHit(e)) {
				blocked.push(e);
			}

		state =	switch (state) {
			case (Stand | Run):

				if (M.dist(0, 0, dirx, diry) > 0.05) {
					dx = dirx * speed * tmod;
					dy = diry * speed * tmod;
					Run;
				}
				else {
					Stand;
				}


			case Dash (dirx, diry):
				if (cd.has("dashTime")) {
					dx = dirx * dashSpeed * tmod;
					dy = diry * dashSpeed * tmod;
					Dash (dirx, diry);
				}
				else
					Stand;
			case Block:
				switch (blocked) {
					case [] if (ca.xDown()):
						Block;
					case []: Stand;
					case _ if (ca.xPressed()):
						 hasUnblock = false;
						 ca.rumble(0.2, 0.1);
						 Game.ME.slowMo();
						 dx = 0;
						 dy = 0;
						 Pary;
					case _:
						 Block;
				}

			case Pary if (!Game.ME.isSlowMo()):
				Stand;

			case Pary:
				if (M.dist(0, 0, dirx, diry) > 0.75) {
					Game.ME.stopSlowMo();
					startDash(dirx, diry);
				}
				else
					Pary;
		};

	}

	override function postUpdate() {
		super.postUpdate();


		for (e in oldBlocked)
			if (!checkHit(e)) {
				this.bump(e.dx,e.dy);
				e.block(this);
			}

		oldBlocked = [];
		for (e in Projectile.ALL)
			if (checkHit(e))
				if (state == Block || state == Pary)
					e.block(this);
				else
					oldBlocked.push(e);

		if (oldState != state) {
			spr.removeChildren();
			var g = new h2d.Graphics(spr);
			switch (state) {
				case Dash (_):
					radius = Const.GRID;
				case Pary:
					radius = Const.GRID*0.5;
					g.beginFill(0x00ff00);
				default:
					radius = Const.GRID*0.5;
					g.beginFill(0xff00ff);
			}
			g.drawCircle(0,0,radius);
		}

		oldState = state;
	}
}
