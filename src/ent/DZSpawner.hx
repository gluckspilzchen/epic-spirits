package ent;

class DZSpawner extends Entity {

	var slocx : Int;	
	var slocy : Int;	

	var basex : Int;
	var tick : Float;
	
	var spawndz : Float;
	var offset : Int;
	var amplitude : Int;

	public function new (x, y) {
		super(x, y);

		basex = x;
		slocx = x;
		slocy = y;

		var dzdata = Data.entities.get(dzspawner);
		spawndz = dzdata.spawncd;
		offset = dzdata.offset;
		amplitude = dzdata.amplitude;

		tick = 0;

		spawnDeathZone();
		cd.setS("spawndz", spawndz);
	}

	function spawnDeathZone() {
		new DeathZone(slocx, slocy);

		slocy += 2 * offset;
		tick++;
		slocx = basex + amplitude * Math.round(Math.sin(tick*offset));
	}


	override function update() {
		super.update();


		if(!cd.hasSetS("spawndz", spawndz))
			spawnDeathZone();

		if(!level.isValid(slocx, slocy)) {
			slocx = basex;
			slocy = 0;
		}
	}
}
