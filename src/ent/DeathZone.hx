package ent;

enum DZState {
	Spawning;
	Active;
}

class DeathZone extends Entity {
	public static var ALL : Array<DeathZone> = [];
	var spawnTime : Float;
	var lifeSpan : Float;

	var state : DZState;

	public function new (x, y) {
		super(x, y);

		ALL.push(this);

		hei *= 2;
		radius *= 2;
		
		var dzdata = Data.entities.get(deathzone);
		spawnTime = dzdata.spawnTime;
		lifeSpan = dzdata.lifeSpan;

		cd.setS("spawning", spawnTime);

		state = Spawning;

		var g = new h2d.Graphics(spr);
		g.beginFill(0xff00ff);
		g.drawRect(0,0,32,32);
	}

	override function update() {
		super.update();

		switch(state){
			case Spawning:
				if (!cd.has("spawning")) {
					state = Active;

					var g = new h2d.Graphics(spr);
					g.beginFill(0xff0000);
					g.drawRect(0,0,32,32);

					cd.setS("lifespan", lifeSpan);
				}
			case Active:
				if (checkHit(Hero.ME))
					Hero.ME.hit();


				if (!cd.has("lifespan")) {
					ALL.remove(this);
					destroy();
				}
		}
	}
}
