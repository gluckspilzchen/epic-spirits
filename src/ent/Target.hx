package ent;

class Target extends Entity {
	public static var ALL : Array<Target> = [];

	private var mob : Mob;

	public function new (x, y, mob) {
		super(x, y);

		ALL.push(this);

		hei *= 2;
		radius *= 2;

		this.mob = mob;

		var g = new h2d.Graphics(spr);
		g.beginFill(0x0000ff);
		g.drawRect(0,0,32,32);
	}

	override function dispose() {
		super.dispose();
		ALL.remove(this);
	}

	public function hit() {
		destroy();
		mob.hit(this);
	}

	override function update() {
		if (checkHit(Hero.ME)) {
			hit();
		}

	}

}
