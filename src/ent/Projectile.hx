package ent;

class Projectile extends Entity {
	public static var ALL : Array<Projectile> = [];
	public var speed(get, null) : Float;

	var dirx : Float;
	var diry : Float;

	inline function get_speed() return speed;

	public function new (x : Int, y : Int, dx : Float, dy : Float) {
		super(x, y);

		ALL.push(this);

		var pdata = Data.entities.get(projectile);
		speed = pdata.speed;
		
		spr.anim.registerStateAnim("rock", 2, 0.10, function() return true );
		spr.setCenterRatio(0.5, 0.5);

		radius *= 2;

		frict = 1;

		dirx = dx;
		diry = dy;
	}

	override function postUpdate() {
		super.postUpdate();
	}

	override function dispose() {
		super.dispose();
		ALL.remove(this);
	}

	public function block(e:Entity) {
		fx.blockedProjectile(0 , 20*speed, centerX, centerY);
		destroy();
	}

	override function update() {
		super.update();

		dx = dirx * speed * tmod;
		dy = diry * speed * tmod;

		if (!level.isValid(cx, cy))
			destroy();

	}

}
