package ent;

import haxe.ds.Option;

enum SpawnPattern {
	Spray;
}

class PSpawner extends Entity {

	var dirx : Float;
	var diry : Float;
	var spawncd : Float;

	public var action : Option<SpawnPattern>;

	public function new (x, y) {
		super(x, y);


		var dzdata = Data.entities.get(pspawner);
		spawncd = dzdata.spawncd;

		cd.setS("spawncd", spawncd);
	}

	public function scheduleSprayPattern (dx, dy) {
		action = Some (Spray);
		dirx = dx;
		diry = dy;
	}

	function gNoise (min:Float, max:Float) {
		var r1 = Math.random();
		var r2 = Math.random();

		var range = Math.abs((max-min)/2);
		r1 = r1*range + min/2;
		r2 = r2*range + min/2;

		return r1 + r2;
	}

	function sprayPattern () {
		var pnumber = 5;
		var spread = 5;

		for (i in 1...5) {
			var ang = Math.atan2(diry, dirx);
			ang += gNoise(-Math.PI/4, Math.PI/4);
			var pdirx = Math.cos(ang);
			var pdiry = Math.sin(ang);

			new Projectile(this.cx, this.cy, pdirx, pdiry);
		}
	}

	override function update() {
		super.update();

		switch (action) {
			case Some (Spray):
				sprayPattern();
			case None :
				null;
		}

		action = None;
	}
}
