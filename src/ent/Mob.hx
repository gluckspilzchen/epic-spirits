package ent;

import haxe.ds.Option;

enum MState {
}

typedef MAttackAction =
	{ predicate : (Entity) -> Bool
	, action : (Entity, Entity) -> Void
	}

enum MAttack {
	Projectile;
}

class Mob extends Entity {
	private var headOffsetX =  2;
	private var headOffsetY =  2;
	private var legsOffsetX = -2;
	private var legsOffsetY =  2;
	private var bodyOffsetX =  2;
	private var bodyOffsetY = -2;

	var currentAttack : MAttack;
	var attacks : Array<MAttackAction>;
	var pspawner : PSpawner;
	var head : Option<Target>;
	var legs : Option<Target>;
	var body : Option<Target>;

	var attackcd : Float;

	public function new (x, y) {
		super(x, y);

		attacks = [];
		var projectile =
			{ predicate : function(e) { return true; }
		        , action : function(e:Entity, h:Entity) {
				var dirx = h.footX - e.footX;
				var diry = h.footY - e.footY;
				var norm = Math.sqrt(dirx * dirx + diry * diry);
				dirx /= norm;
				diry /= norm;
				pspawner.scheduleSprayPattern(dirx, diry); }
			};
		attacks.push(projectile);

		var mdata = Data.entities.get(mob);
		attackcd = mdata.attackcd;

		pspawner = new PSpawner(x, y);

		head = Some (new Target(x + headOffsetX, y + headOffsetY, this));
		legs = Some (new Target(x + legsOffsetX, y + legsOffsetY, this));
		body = Some (new Target(x + bodyOffsetX, y + bodyOffsetY, this));

		cd.setS("attackcd", attackcd);
	}

	override function dispose() {
		super.dispose();
		pspawner.destroy();
	}

	public function hit(e:Target) {
		var isAlive = false;
		switch (head) {
			case Some (t) if (t == e): head = None;
			case Some (_): isAlive = true;
			default: null;
		}

		switch (legs) {
			case Some (t) if (t == e): legs = None;
			case Some (_): isAlive = true;
			default: null;
		}

		switch (body) {
			case Some (t) if (t == e): body = None;
			case Some (_): isAlive = true;
			default: null;
		}

		if (!isAlive) {
			destroy();
			Game.ME.bumpScore();
		}

	}

	function performAttack() {
		var attacks = [ for (e in this.attacks) if (e.predicate(this)) e ];
		var attack = attacks[Math.floor(Math.random() * attacks.length)];
		attack.action(this, Hero.ME);
	}

	override function update() {
		super.update();

		pspawner.cx = this.cx;
		pspawner.cy = this.cy;

		if(!cd.hasSetS("attackcd", attackcd))
			performAttack();
	}
}
